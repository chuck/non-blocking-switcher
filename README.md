# Non-Blocking Switcher

This extension works around [bug 2057](https://gitlab.gnome.org/GNOME/gnome-shell/issues/2057)
in the Gnome Shell window switcher (`cycle-windows` & `cycle-windows-backward` -- bound
to `<Super>Esc` by default) where other keystrokes are blocked while the modifier key is
held down.

This allows you to

1. Press and hold `<Super>`
2. Press `Esc` to begin a window switch.
3. Press `1` to change your mind about the window switch and jump to workspace 1 (`<Super>1`)
4. Release `<Super>`

Without this extension, you must release and re-press `<Super>` between steps 2 and 3.

This extension works by immediately ending the application-switch as soon as it has started.
*This means that you can only ever switch to the first other-window in the tab list!*
The default tab list order is most-recently-used, so by default ***you can only switch
between two windows!*** Thus, it is highly recommended to use the [Round Robin Tab Order
extension](https://gitlab.gnome.org/chuck/round-robin-tab-order) with this extension so that
all windows are reachable.
