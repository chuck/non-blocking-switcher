/* nonblockingswitcher: Application switching oughtn't stop all other keyboard shortcuts
 * Copyright (C) 2019 Scott Worley
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * or version 3 of the License (at your option).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

const AltTab = imports.ui.altTab;

let oldShow;

function init() {
    oldShow = AltTab.WindowCyclerPopup.prototype.show;
}

function enable() {
    AltTab.WindowCyclerPopup.prototype.show = function (backward, binding, mask) {
        oldShow.call(this, backward, binding, mask);
        this._finish();
        return false;
    };
}

function disable() {
    AltTab.WindowCyclerPopup.prototype.show = oldShow;
}

